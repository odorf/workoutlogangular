(function() {
	angular
		.module('workoutlog.log', ['ui.router'])
		.config(logConfig);

	logConfig.$inject = [ '$stateProvider' ];
	function logConfig($stateProvider) {
		$stateProvider
			.state('log', {
				url: '/log',
				templateUrl: '/components/log/log.html',
				controller: LogController,
				controllerAs: 'ctrl',
				bindToController: this,
				resolve: {
					getUserDefinitions: [ 
						'DefineService',
						function(DefineService) {
							return DefineService.fetch();
					}]
				}
			});
	}

	LogController.$inject = [ 'DefineService', 'LogsService' ];
	function LogController( DefineService, LogsService){
		var vm = this;
		vm.log = {};
		vm.userDefinitions = DefineService.get();
		vm.save = function() {
			LogsService.create(vm.log);
		};
	}

})();