(function() {
	angular
		.module('workoutlog')
		.service('LogsService', LogsService);

	LogsService.$inject = ['$http', 'API_BASE'];

	function LogsService($http, API_BASE) {
		var svc = this;

		svc.history = [];

		svc.create = function(log) {
			return $http.post(API_BASE + "log", {
				log: log
			}).then(function(response) {
				svc.history.push(response.data);
			});
		};

		svc.fetch = function() {
			return $http.get(API_BASE + "log")
			.then(function(response) {
				svc.history = response.data;	
			});
		};

		svc.get = function() {
			return svc.history;
		};
	}
})();