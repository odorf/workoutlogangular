(function() {
  angular.module("workoutlog")
    .service("CurrentUser", [ "$window", function($window){
      var CurrentUser = (function() {
        function CurrentUser() {
          var curr = $window.localStorage.getItem("currentUser");
          if (curr && curr !== "undefined"){
            this.currentUser = JSON.parse($window.localStorage.getItem("currentUser"));
          }
        }
        CurrentUser.prototype.set = function(user) {
          this.currentUser = user;
          $window.localStorage.setItem("currentUser", JSON.stringify(this.currentUser));
        };
        CurrentUser.prototype.get = function() {
          return this.currentUser || {};
        };
        CurrentUser.prototype.clear = function() {
          this.currentUser = undefined;
          $window.localStorage.removeItem("currentUser");
        };
        CurrentUser.prototype.isSignedIn = function() {
          return !!this.get().id;
        };
        return CurrentUser;
      })();
      
      return new CurrentUser();

    }]);
})();