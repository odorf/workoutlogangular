angular.module('workoutlog')
  .service('UsersService', [
    '$http', 'API_BASE', 'SessionToken', 'CurrentUser',
     function($http, API_BASE, SessionToken, CurrentUser) {

      function UsersService() {
      }

      UsersService.prototype.create = function(user) {
        var userPromise = $http.post(API_BASE + 'user', {
          user: user
        })
        .then(function(response) {
          SessionToken.set(response.data.sessionToken);
          CurrentUser.set(response.data.user);
        });
        return userPromise;
      };
      UsersService.prototype.login = function(user) {
        var userPromise = $http.post(API_BASE + 'login',{ user: user });
        userPromise.then(function(response) {
            SessionToken.set(response.data.sessionToken);
            CurrentUser.set(response.data.user);
        });
        return userPromise;
      };
      return new UsersService();

  }]);
